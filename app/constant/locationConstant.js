
export default {
	// general menu
	CREATE_QUESTION: '/create-question',
	CREATE_POLL: '/create-poll',
	CREATE_EVENT: '/create-event',
	CREATE_TOPIC: '/create-topic',

	// user menu
	'PROFILE': '/profile',
	'ACCOUNT':'/account',

	//

	'LOGIN': '/login',
	'REGISTER': '/register'

	
}
