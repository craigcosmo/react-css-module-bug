import React from 'react'
import 'registerForm.scss'
import { intlShape, injectIntl, defineMessages } from 'react-intl'
import Tooltip from 'react-tooltip'
import {findDOMNode} from 'react-dom'
import validate from 'validate'
import {Link} from 'react-router'
import L from 'locationConstant'

const messages = defineMessages({
	name: {
		id: 'registerForm.name',
		defaultMessage: 'full name'
	},
	email: {
		id: 'registerForm.email',
		defaultMessage: 'email address'
	},
	password: {
		id: 'registerForm.password',
		defaultMessage: 'password'
	},
	passwordConfirm: {
		id: 'registerForm.passwordConfirm',
		defaultMessage: 'confirm password'
	}
})

class RegisterForm extends React.Component{
	constructor(props) {
		super(props)
		this.onFormSubmit = this.onFormSubmit.bind(this)
		this.onNameChange = this.onNameChange.bind(this)
		this.onPasswordChange = this.onPasswordChange.bind(this)
		this.onPasswordConfirmChange = this.onPasswordConfirmChange.bind(this)
		this.onEmailChange = this.onEmailChange.bind(this)

		this.name = ''
		this.email = ''
		this.password = ''
		this.passwordConfirm = ''

		this.state = {
			registerModal: false,
			nameErrorMessage: '',
			emailErrorMessage: '',
			passwordErrorMessage: '',
			passwordConfirmErrorMessage: '',
		}

	}
	hideErrorToolTip(){
		Tooltip.hide(findDOMNode(this.refs.name))
		Tooltip.hide(findDOMNode(this.refs.email))
		Tooltip.hide(findDOMNode(this.refs.password))
		Tooltip.hide(findDOMNode(this.refs.passwordConfirm))
	}
	onFormSubmit(e){
		e.preventDefault()
		this.hideErrorToolTip()

		let input = [
			{value:this.name, callback: this.nameError.bind(this) },
			{value:this.email, type:'email', callback: this.emailError.bind(this) },
			{value:this.password, min:6, callback: this.passwordError.bind(this) },
			{
				value: this.passwordConfirm, 
				value2: this.password, 
				type: 'passwordConfirm', 
				callback: this.passwordConfirmError.bind(this)
			}
		]
		if (validate(input)) {

			let regData = {
				email: this.email,
				password: this.password,
				name: this.name
			}
			this.props.onData(regData)
		}
		
	}
	nameError(reason){
		if (reason === 'length') this.setState({nameErrorMessage : 'required'}) 
		
	}
	emailError(reason){
		if (reason === 'length') this.setState({emailErrorMessage : 'required'}) 
		if (reason === 'valid') this.setState({emailErrorMessage : 'email is invalid'}) 
		
	}
	passwordError(reason){
		if (reason === 'length') this.setState({passwordErrorMessage : 'required'}) 
		if (reason === 'min') this.setState({passwordErrorMessage : 'minimum 6 characers'}) 
		
	}
	passwordConfirmError(reason){
		if (reason === 'length') this.setState({passwordConfirmErrorMessage : 'required'}) 
		if (reason === 'match') this.setState({passwordConfirmErrorMessage : 'not match password'}) 
		
	}
	onPasswordConfirmChange(e){
		this.passwordConfirm = e.target.value
	}
	onPasswordChange(e){
		this.password = e.target.value
	}
	onNameChange(e){
		this.name = e.target.value
	}
	onEmailChange(e){
		this.email = e.target.value
	}
	renderError(message){
		return <div className="error">{message}</div>
	}
	render(){ 
		return(
			<form onSubmit={this.onFormSubmit} styleName="form">
				<div className="relative mb20">
					<input 
						ref= "name"
						className="ui-input-text" 
						type="text" 
						placeholder={this.props.intl.formatMessage(messages.name)} 
						onChange={this.onNameChange}
						data-tip
						data-for="name-tip" />
					<i className="fa fa-user" aria-hidden="true"></i>
					{this.state.nameErrorMessage && this.renderError(this.state.nameErrorMessage)}
				</div>
				<div className="relative mb20">
					<input 
						ref= "email"
						className="ui-input-text" 
						type="text" 
						placeholder={this.props.intl.formatMessage(messages.email)} 
						onChange={this.onEmailChange}
						data-tip
						data-for="email-tip" />
					<i className="fa fa-envelope mail-icon" aria-hidden="true"></i>

					{this.state.emailErrorMessage && this.renderError(this.state.emailErrorMessage)}
				</div>
				
				<div className="relative mb20">
					<input 
						ref= "password"
						className="ui-input-text" 
						type="password" 
						placeholder={this.props.intl.formatMessage(messages.password)} 
						onChange={this.onPasswordChange}
						data-tip
						data-for="password-tip" />
					<i className="fa fa-lock lock-icon" aria-hidden="true"></i>

					{this.state.passwordErrorMessage && this.renderError(this.state.passwordErrorMessage)}
				</div>
				<div className="relative mb20">
					<input 
						ref= "passwordConfirm"
						className="ui-input-text" 
						type="password" 
						placeholder={this.props.intl.formatMessage(messages.passwordConfirm)} 
						onChange={this.onPasswordConfirmChange}
						data-tip
						data-for="password-confirm-tip" />
					<i className="fa fa-lock lock-icon" aria-hidden="true"></i>

					{this.state.passwordConfirmErrorMessage && this.renderError(this.state.passwordConfirmErrorMessage)}
				</div>
				<div className="mb20">
					<button styleName="register-btn">register</button>
				</div>
				<div className="top-border">
					<div className="row">
						<div className="col-xs-7 vac">
							<Link to="/login" className="ib vam">Already have an account?</Link>
						</div>
						<div className="col-xs-5 right vac">
							<Link className="ib vam unselectable" to={L.LOGIN}>Login</Link>
						</div>
					</div>
				</div>
			</form>
		)
	}
}



RegisterForm.propTypes = {
    intl: intlShape.isRequired,
}

export default injectIntl(RegisterForm)