import React from 'react'
import {Link} from 'react-router'
import Header from 'Header'
import 'forgotPassword.scss'

export default class ForgotPassword extends React.Component {
	constructor(){
		super()
	}
	render(){
		return (
			<div className="forgot-password">
				<Header {...this.props} />
				<main>
					<input type="text" placeholder="email" />
					<button>send</button>
				</main>
			</div>
		)
	}
}