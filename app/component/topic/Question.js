import React from 'react'
import 'question.scss'


const Question = props => {
	return(
		<div className="question-header">
			<div className="question-selection-wrapper flex">
				<div className="leftcol"><i className="fa fa-question" aria-hidden="true"></i></div>
				<div className="vac rightcol flex1"><h3 className="title cap">{props.item.title}</h3></div>
			</div>
		</div>
		
	)
}
export default Question