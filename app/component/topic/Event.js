import React from 'react'
import {Component} from 'react'
import 'event.scss'
import UserSection from 'UserSection'
import moment from 'moment'


export default class Event extends Component{
	constructor(props){
		super(props)
		// console.log('alalala', props)
		this.data = {
			status: '',
			topicId: props.item.topicId
		}
	}
	getValueAndSubmit(value){
		this.data.status = value
		this.props.submitAttending(this.data)
		// this.props.updateAttending(this.data)
		.then( () =>{
			console.log('finish')
		})
	}
	renderOption(){
		return (
			<div className="event-action flex">
				{	this.props.item.eventOption && 
					this.props.item.eventOption.map( (item, index) => {
					return (
						<div key={index} className="event-action-item" onClick={this.getValueAndSubmit.bind(this, item.value)}>
							<span className="totalCount">{item.totalCount}</span>
							<span className="label">{item.label}</span>
						</div>
					)
				})}
			</div>
		)
	}

	render(){ 
		return(
			<div className="event-header">
				<div className="flex">
					<div className="leftcol">
						<div className="vac max-height">
							<div>
								<span className="day-of-week">{moment.unix(this.props.item.eventData.startDate).format('dddd')}</span>
								<span className="date-number">{moment.unix(this.props.item.eventData.startDate).format('D')}</span>
								<span className="month">{moment.unix(this.props.item.eventData.startDate).format('MMMM')}</span>
							</div>
						</div>
					</div>
					<div className="rightcol relative">
						<div className="" style={{marginBottom:'3px'}}>
							<i className="fa fa-clock-o" aria-hidden="true"></i>
							<span className="time">{this.props.item.eventData.startTime}</span>
							<span className="hy">-</span>
							<span className="time">{this.props.item.eventData.endTime}</span>
						</div>
						<div>
							<i className="fa fa-map-marker event-marker" aria-hidden="true"></i>
							<span className="location ib cap">{this.props.item.location}</span>
						</div>
						{this.renderOption()}
					</div>
				</div>
				
			
				
				
			</div>
		)
	}
}
Event.propTypes = {
  item: React.PropTypes.object.isRequired,
  submitAttending: React.PropTypes.func.isRequired,
}
