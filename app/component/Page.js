import React from 'react'
// global style
import '../style/globalStyle/normalize.scss'
import '../style/globalStyle/flexboxgrid.scss'
import '../style/globalStyle/fontAwesome.scss'
import '../style/globalStyle/font.scss'
import '../style/globalStyle/page.scss'
import '../style/globalStyle/ui.scss'
import '../style/globalStyle/media.scss'
import '../style/globalStyle/gap.scss'
// global component
import '../style/globalComponent/select.scss'
import '../style/globalComponent/tooltip.scss'





export default class Page extends React.Component {
	render() {
		return (
			<div className="page">
				{React.cloneElement(this.props.children, {...this.props})}
			</div>
		)
	}
}

