
import React from 'react'
import {findDOMNode} from 'react-dom'
import {Link} from 'react-router'
import Header from 'Header'
import 'login.scss'
import Modal from 'react-modal'
import ToolTip from 'react-tooltip'
import 'loginErrorModal.scss'

import {validate} from 'validate'
import L from 'locationConstant'
// import 'tooltip.scss'

export default class Login extends React.Component {
	constructor(){
		super()
		this.onPasswordChange = this.onPasswordChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.onEmailChange = this.onEmailChange.bind(this)

		this.email = ''
		this.password = ''

		this.state ={
			loginError: false
		}

	}

	componentDidMount(){
		this.props.listenAuthChange()
		.then( (user) => {
			// console.log('user from change',user)
			this.redirectAfterSuccess()
		})
	}
	handleSubmit(e){
		e.preventDefault()
		if (this.validateForm() === false) return
		this.props.submitLogin({
			email: this.email,
			password: this.password
		})
	}

	redirectAfterSuccess(){
		this.context.router.push('/forum/barista-trade-fair')
	}
	showEmailError(){

	}
	showPasswordError(){

	}
	validateForm(){
		let valid =true

		if (this.email.trim().length===0) {
			ToolTip.show(findDOMNode(this.refs.email))
			valid= false
		}else{
			ToolTip.hide(findDOMNode(this.refs.email))
		}
		if (this.password.length===0) {
			ToolTip.show(findDOMNode(this.refs.password))
			valid= false
		}else{
			ToolTip.hide(findDOMNode(this.refs.password))
		}
		return valid
	}
	closeModal(){
		this.setState({loginError:false})
	}
	onPasswordChange(e){
		this.password = e.target.value
	}
	onEmailChange(e){
		this.email = e.target.value
	}
	render(){
		return (
			<div className="login">

				<main>

					<h1>Log in to your account</h1>
					<form onSubmit={this.handleSubmit}>
						<div className="relative mb20">
							<input className="email text-box" ref="email" type="text" onChange={this.onEmailChange} placeholder="email" data-tip="email is required" data-for="email-tip" />
							<i className="fa fa-envelope mail-icon" aria-hidden="true"></i>
							<ToolTip id="email-tip" place="right" event="show" />
						</div>
						<div className="relative mb20">
							<input className="password text-box" ref="password" type="password" onChange={this.onPasswordChange} placeholder="password" data-tip="password is required" data-for="password-tip" />
							<i className="fa fa-lock lock-icon" aria-hidden="true"></i>
							<ToolTip id="password-tip" place="right" event="show" />
						</div>
						<div className="row mb20">
							<div className="col-xs-6">
								<input id="rem" type="checkbox" />
								<label for="rem" className="capitalize rem unselectable">remember me</label>
							</div>
							<div className="col-xs-6 right">
								<Link className="forgot-password-link">Forgot password?</Link>
							</div>
						</div>
						<div className="login-sep mb20">
							<button type="submit" className="login-btn">login</button>
						</div>
						<div className="row">
							<div className="col-xs-7 vac">
								<Link to="/register" className="reg-link ib vam">Don't have an account?</Link>
							</div>
							<div className="col-xs-5 right vac">
								<Link className="signup-btn ib vam unselectable" to={L.REGISTER}>Sign up</Link>
							</div>
						</div>
					</form>

				</main>
				<Modal
					overlayClassName="login-error-modal-overlay"
					className="login-error-modal"
					isOpen={this.state.loginError} 
					onRequestClose={this.closeModal.bind(this)}
					contentLabel="modal">
					<span className="login-error">email or password incorrect</span>
				</Modal>
			</div>

		)
	}
}

Login.contextTypes = {
  router: React.PropTypes.object.isRequired
}