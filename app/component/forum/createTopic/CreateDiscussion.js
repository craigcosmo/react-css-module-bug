import React from 'react'
import Header from 'Header'
import 'createDiscussion.scss'
import validate from 'validate'
import {HotKeys} from 'react-hotkeys'
import PostButtons from 'PostButtons'
import Tool from 'Tool'

export default class CreateDiscussion extends React.Component {
	constructor(props) {
		super(props)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.titleOnChange = this.titleOnChange.bind(this)
		this.detailOnChange = this.detailOnChange.bind(this)
		this.state = {
			title: '',
			detail: ''
		}
	}
	componentDidUpdate(){
		
	}
	titleError(){
		console.log('missing title')
	}
	detailError(){
		console.log('missing detail')
	}
	handleSubmit(e){
		e.preventDefault()
		let input = [
			{ field: 'title', value:this.state.title, callback: this.titleError.bind(this) },
			{ field: 'detail', value:this.state.title, callback: this.detailError.bind(this) }
		]
		if (validate(input)) {
			let topicData = {
				title: this.state.title,
				type: 'discussion',
			}
			this.props.submitTopic(topicData)
			.then((key) =>{
				
				let postData = {
					message: this.state.detail,
					topicId: key,
				}
				this.props.submitPost( postData).then( () => {
					this.setState({
						title: '',
						detail: ''
					})
				})
			})
		}
	}
	detailOnChange(e){
		this.setState({
			detail: e.target.value
		})
	}
	titleOnChange(e){
		this.setState({
			title: e.target.value
		})
	}
	render(){
		const keyMap = {
			'submitForm': 'command+enter'
		}
		const handlers = {
			'submitForm': this.handleSubmit
		}

		return (
			<div className="create-topic">
				<form className="forum-form" onSubmit={this.handleSubmit}>
					<HotKeys keyMap={keyMap} handlers={handlers}>
						<input className="topic-title" value={this.state.title} type="text" placeholder="Topic" onChange={this.titleOnChange} />
						<div>
							<textarea value={this.state.detail} placeholder="Share what's in your mind" onChange={this.detailOnChange}></textarea>
						</div>
						<Tool />
						<div>
							<PostButtons />
						</div>
					</HotKeys>
				</form>
			</div>
		)
	}
}

CreateDiscussion.contextTypes = {router: React.PropTypes.object.isRequired}