import React from 'react'
import 'tool.scss'
export default class Tool extends React.Component{
	constructor(){
		super()
	}
	render(){ 
		return(
			<div className="tool">
				<span><i className="fa fa-picture-o" aria-hidden="true"></i></span>
				<span><i className="fa fa-link" aria-hidden="true"></i></span>
				<span><i className="fa fa-video-camera" aria-hidden="true"></i></span>
			</div>
		)
	}
}	