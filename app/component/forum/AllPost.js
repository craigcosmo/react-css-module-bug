import React from 'react'
import Entry from 'Entry'
import R from 'ramda'

export default class AllPost extends React.Component{
	constructor(props){
		super(props)
		// remove first post if this topic is discussion
		if (props.item.post && props.item.type ==='discussion') {
			this.posts = R.clone(props.item.post)
			this.posts.shift()
		}
	}
	skipRenderFirstPost(item, index){
		// console.log('d',index)
		// console.log('type', this.props.item.type)
		if (this.props.item.type==='discussion' && index > 0) {
			return(
				<div className="post-item" key={index}>
					<Entry 
						item={item}
						submitReply = {this.props.submitReply}
						getTopic={this.props.getTopic} />
				</div>
			)
		}else if(this.props.item.type !== 'discussion'){
			return(
				<div className="post-item" key={index}>
					<Entry 
						item={item}
						submitReply = {this.props.submitReply}
						getTopic={this.props.getTopic} />
				</div>
			)
		}
		// else{
		// 	return(
		// 		<div className="post-item" key={index}>
		// 			<Entry 
		// 				item={item}
		// 				submitReply = {this.props.submitReply}
		// 				getTopic={this.props.getTopic} />
		// 		</div>
		// 	)
		// }
	}
	render(){ 
		return(
			<div className="all-post">
				{	this.props.item.post && 
					// this.posts.map( (item, index) => (
					this.props.item.post.map( (item, index) => (
						this.skipRenderFirstPost(item, index)
					))
				}
			</div>
		)
	}
}

AllPost.propTypes ={
	item: React.PropTypes.object.isRequired,
	submitReply: React.PropTypes.func.isRequired,
}