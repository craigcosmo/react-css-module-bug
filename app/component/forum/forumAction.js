import i from 'actionConstant'
import config from 'config'
import request from 'request'
import R from 'ramda'
import {makeUri} from 'uriHelper'
import moment from 'moment'
import user from 'user'
import { push } from 'react-router-redux'
import {objectToArray} from 'objectHelper'
import V from 'valid-url'
import youtubeUrl from 'youtube-url'
import React from 'react'

function makeYoutubeVideo(message){
	let id = youtubeUrl.extractId(message)
	return 	(
		<div className="video-container">
			<iframe width="300" height="169" src={'https://www.youtube.com/embed/'+id} style={{border:0}} />
		</div>
	)
}
function makeArticle(message){
	return(
		<article>
			<div className="title"><a href={message.url} target="_blank">{message.title}</a></div>
			<div className="image"><a href={message.url}><img src={message.image} /></a></div>
			<div className="description">{message.description}</div>
		</article>
	)
}

/**
 * modify value of message items in array, turn a youtube link to iframe jsx
 * @param  {array}
 * @return {array}
 */
function getYoutubeVideo(array){
	let modifiedArray = array
	modifiedArray.map( item => {
		// console.log('iii',item.message)
		if (youtubeUrl.valid(item.message) ) {
			// console.log('iii',item.message)
			item.message = makeYoutubeVideo(item.message)
		}
	})
	return modifiedArray
}
/**
 * if message values are link, get ogp data then save back into array
 * @param  {array}
 * @param  {Function}
 * @return {callback wtih param is new modified array}
 */


function sortDecending(payload){
	let sortByCreated = R.sortBy( R.prop('created'))
	payload = sortByCreated(payload)
	// console.log(payload)
	return payload.reverse()
}
function ReplyObjectToArray(data){
	// check id data is array
	if(data.constructor === Array){
		data.map( (item) => {
			if (item.hasOwnProperty('reply')) {
				let replies = item.reply
				let array = []
				for ( let i in replies){
					replies[i].toMessageId = i
					array.push(replies[i])
				}
				return item.reply = array
			}
		})
		return data
	}else{
		return data
	}
}
function formatPollData(topicData){
	if (topicData.type === 'poll') {
		topicData.votedByThisUser = false
		topicData.voter = objectToArray(topicData.voter)

		let voteResultArray = []

		// console.log('checking out voet', topicData.voter)
		
		topicData.voter.forEach(function(item){
			// console.log('doing it')
			if (item.userId == user().userId ) {
				// identify if current user is also voter
				topicData.votedByThisUser = true
				// console.log('ohchalala')
			} 	
		})

		topicData.option.forEach(function(optionVal, index){

			let numberOfVote = 0
			topicData.voter.forEach(function(io){
				if(io.label === optionVal){
					numberOfVote = numberOfVote +1
				}
			})
			voteResultArray[index] = {}
			voteResultArray[index].label = optionVal
			voteResultArray[index].vote = numberOfVote
		})
		/*
		voteResultArray = [{label: label1, numOfVote: 50}, {label: la, numOfVote: 1}]
		*/
		topicData.pollResult = voteResultArray
	}
	return topicData
}


