import React from 'react'
import Header from 'Header'
import Select from 'react-select'
import 'reactSelect.scss'
import 'createPoll.scss'
import 'createPollSelect.scss'
import validate from 'validate'
import {hasDuplicateItem} from 'objectHelper'
import Modal from 'react-modal'

import PostButtons from 'PostButtons'


export default class CreatePoll extends React.Component{
	constructor(){
		super()
		this.state = {
			type: 'multipleChoice',
			selected: 'multiple',
			title : '',
			option: 2,
			modal:false
		}
		this.selectOnChange = this.selectOnChange.bind(this)
		this.handleSubmit= this.handleSubmit.bind(this)

	}
	titleOnChange(e){
		this.setState({title:e.target.value})
	}
	addChoice(e){
		e.preventDefault()
		const max = 10
		this.setState({option: this.state.option< max ? this.state.option +1 : this.state.option })
	}
	removeChoice(){
		const min = 2
		this.setState({option: this.state.option > min ? this.state.option -1 : this.state.option })
	}
	selectOnChange(val){
		console.log(val.value)
		this.setState({
			selected: val
		})
	}
	showTitleError(reason){
		console.log('title ' + reason)
	}
	showOptionError(reason) {
		console.log('option ' +reason)
	}
	handleSubmit(e){

		e.preventDefault()
		// console.log(this.getOptionValues())
		// validate option
		if(hasDuplicateItem(this.getOptionValues()) ) {
			this.setState({modal:true})
			return
		}

		console.log(this.state.title)
		let input = [
			{
				field   : 'title', 
				value   : this.state.title, 
				callback: this.showTitleError.bind(this)	
			},
			{
				field: 'option',
				value: this.getOptionValues().length >= 2 ? true : '',
				callback: this.showOptionError.bind(this)
			}
		]

		if (validate(input)) {
			let data = {
				title   : this.state.title,
				pollType: this.state.type,
				type    : 'poll',
				option  : this.getOptionValues()
			}
			console.log(data)
			console.log(this.getOptionValues())
			this.props.submitTopic(data).then( (topicId) =>{
			})
		}
	}
	getOptionValues(){
		let collection = []

		for (let i = 0; i <this.state.option; i++) {
			let el = document.getElementsByClassName('abcd')[i]
			let input = el.querySelector('input')
			if (input.value.trim()) {
				collection.push(input.value.trim())
			}
		}
		console.log(collection)
		return collection
	}
	closeModal(){
		this.setState({modal:false})
	}
	renderOption(){
		let a = []

		for (let i = 0; i < this.state.option; i++) {
			a.push(
				<div className="item relative abcd" key={i}>
					<input type="text" placeholder="Add a Choice" />
					<span className="delete" onClick={this.removeChoice.bind(this)}></span>
				</div>
			)
		}

		if (this.state.type === 'multipleChoice') {
			return (
				<div className="mutiple-choice-wrapper">
					{a}
				</div>
			)
		}
	}
	render(){ 
		
		const selectItem = [
			{ value: 'multiple', label: 'multiple choice' },
			{ value: 'yesno', label: 'yes or no' }
		]
		return(
			<div className="create-poll">
				<form className="poll-form" onSubmit={this.handleSubmit}>
					<input type="text" placeholder="What are polling about?" className="poll-title" onChange={this.titleOnChange.bind(this)} />
					<h2 className="cap hide">poll type</h2>
					<Select
						name="sort"
						value={this.state.selected}
						options={selectItem}
						onChange={this.selectOnChange}
						clearable={false}
						searchable = {false} />
					<div className="option-wrapper">
						<h5 className="cap mb20">options</h5>
						{this.renderOption()}
						<div>
							<button className="add-btn" title="add choice" onClick={this.addChoice.bind(this)}></button>
						</div>
					</div>
					<PostButtons />
				</form>
				<Modal
					overlayClassName="login-error-modal-overlay"
					className="login-error-modal"
					isOpen={this.state.modal} 
					onRequestClose={this.closeModal.bind(this)}
					contentLabel="modal">
					<span className="login-error">you have duplicated option. option should be unique</span>
				</Modal>
			</div>		
		)
	}
}
CreatePoll.contextTypes = {router: React.PropTypes.object.isRequired}