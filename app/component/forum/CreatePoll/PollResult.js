import React from 'react'
import UserSection from 'UserSection'
import 'pollResult.scss'
import user from 'user'
import {objectToArray} from 'objectHelper'


const PollResult = props => (
	<div className="poll-result">
		<div className="poll-selection-wrapper">
			<div className="poll-selection">
				{
					props.item.pollResult.map( (item, index) => (
						<div key={index} className="poll-selection-item flex">
							<div className="leftcol">
								<span style={{'marginRight':'300px'}}>{item.label}</span>
							</div>
							<div className="right rightcol">
								<span className="vote-num">{item.vote}</span>
								{item.vote >1 && <span>votes</span>}
								{item.vote == 1 && <span>vote</span>}
								{item.vote == 0 && <span>vote</span>}
								<span className="vote-percent">({parseInt(item.vote) * 100/props.item.voter.length+'%'})</span>
							</div>
						</div>
					))
				}
			</div>
		</div>		
	</div>
)

export default PollResult