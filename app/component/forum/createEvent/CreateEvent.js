import React from 'react'
import Header from 'Header'
import 'createEvent.scss'
import Calendar from 'Calendar'
import validate from 'validate'
import moment from 'moment'
import Time from 'Time'
import PostButtons from 'PostButtons'



export default class CreateEvent extends React.Component{
	constructor(props){
		super(props)
		// console.log(props)
		this.state = {
			title: '',
			location:'',
			start:'',
			end:'',
			startDate: props.event.startDate,
			endDate: props.event.endDate,
			startTime: props.event.startTime,
			endTime: props.event.endTime,
			// startTime: '',
			// endTime: ''
			titleErrorMessage: '',
			endDateErrorMessage:'',
			locationErrorMessage:''
		}
	}
	componentDidMount(){
		// this.props.storeStartDate(new Date())
		// this.props.storeEndDate(this.state.endDate)
	}
	componentDidUpdate(){
		if (this.props.event.startDate > this.props.event.endDate && this.props.event.endDate !== null){
			this.props.storeEndDate(null)
		}
	}
	startDateOnChange(date){
		this.props.storeStartDate(date)
	}
	endDateOnChange(date){
		this.props.storeEndDate(date)
	}
	showTitleError(reason){
		if (reason === 'length') this.setState({titleErrorMessage:'required'})
		// alert('missing title')
	}
	showEndError(reason){
		console.log('end date eror')
		if (reason === 'length') this.setState({endDateErrorMessage:'required'})
	}
	showStartError(){
		
	}
	showLocationError(reason){
		if (reason === 'length') this.setState({locationErrorMessage:'required'})
		console.log('missing location')
		// alert('missing location')
	}
	titleOnChange(e){
		this.setState({title: e.target.value})
	}
	renderError(message){
		return <span className="event-error">{message}</span>
	}
	DateObjectToSecond(ob){
		
		return moment(ob).unix()
	}
	locationChange(e){
		this.setState({location:e.target.value})
	}
	handleSubmit(e){
		e.preventDefault()
		// console.log(this.getOptionValues())
		// console.log(this.state.title)
		console.log('mockkc', this.props.event.endDate)
		this.setState({
			titleErrorMessage: '',
			endDateErrorMessage:''
		})
		let input = [
			{
				field   : 'title',
				value   : this.state.title,
				callback: this.showTitleError.bind(this)
			},
			{
				field   : 'end',
				value   : this.props.event.endDate,
				callback: this.showEndError.bind(this)
			},
			{
				field   : 'start',
				value   : this.props.event.startDate,
				callback: this.showStartError.bind(this)
			},
			{
				field   : 'location',
				value   : this.state.location,
				callback: this.showLocationError.bind(this)
			}
		]
		console.log(input)
		// console.log(input)
		if (validate(input)) {
			let data = {
				title: this.state.title,
				type : 'event',
				location: this.state.location,
				eventData:{
					startDate:this.DateObjectToSecond(this.props.event.startDate),
					endDate  : this.DateObjectToSecond(this.props.event.endDate),
					startTime: this.props.event.startTime,
					endTime  :this.props.event.endTime
				}
			}
			console.log(data)
			this.props.submitTopic(data).then( topicId =>{

			})
		}
	}

	render(){ 
		return(
			<div className="create-event">
				<form className="create-event-form" onSubmit={this.handleSubmit.bind(this)}>
					<div className="relative title-wrapper">
						<input 
							type="text" 
							className="event-title" 
							onChange={this.titleOnChange.bind(this)} 
							placeholder="Event Name"
							data-tip="required"
							data-for="title"
							ref="title"  />
						{this.state.titleErrorMessage && this.renderError(this.state.titleErrorMessage)}
					</div>
					<div className="event-content-wrapper">
						<div className="start-date-wrapper mb30">
							<div><h6 className="cap mb10">start date:</h6></div>
							<Calendar 
								minDate = {this.props.event.today}
								dateOnChange = {this.startDateOnChange.bind(this)} 
								defaultDate = {this.props.event.startDate}
								placeholder = "start date" />
							<Time 
								time={this.props.event.startTime}
								storeTime = {this.props.storeStartTime} />
						</div>
						<div className="end-date-wrapper mb30 relative">
							<div><h6 className="cap mb10">end date:</h6></div>
							<Calendar 
								minDate = {this.props.event.startDate}
								dateOnChange = {this.endDateOnChange.bind(this)} 
								defaultDate = {this.props.event.endDate}
								placeholder= "end date" />
							{this.state.endDateErrorMessage && this.renderError(this.state.endDateErrorMessage)}
							<Time 
								time={this.props.event.endTime}
								storeTime ={this.props.storeEndTime} />
						</div>
						<div className="end-date-wrapper relative">
							<div><h6 className="cap mb10">location:</h6></div>
							<div className="relative location-wrapper">
								<input className="location-input" type="text" value={this.state.location} onChange={this.locationChange.bind(this)}  />
								<i className="fa fa-map-marker" aria-hidden="true"></i>

							</div>
							{this.state.locationErrorMessage && this.renderError(this.state.locationErrorMessage)}
						</div>
					</div>
					<PostButtons />
				</form>
			</div>
		)
	}
}

CreateEvent.contextTypes = {router: React.PropTypes.object.isRequired}
CreateEvent.propTypes ={
	submitTopic: React.PropTypes.func.isRequired,
	event: React.PropTypes.object.isRequired,
	storeEndTime: React.PropTypes.func.isRequired,
	storeStartTime: React.PropTypes.func.isRequired,
	storeEndDate:React.PropTypes.func.isRequired,
	storeStartDate:React.PropTypes.func.isRequired
}