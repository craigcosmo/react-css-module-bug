import React from 'react'
import Pikaday from 'pikaday'
import 'pikaday.scss'
import 'calendar.scss'

export default class Calendar extends React.Component{
	constructor(){
		super()
		this.cal
	}
	componentDidMount(){
		this.initCalendar()
	}
	componentDidUpdate(){
		this.cal.destroy()
		this.initCalendar()
		// console.log('min',this.props.minDate)
		// console.log('defaul', this.props.defaultDate)
	}
	componentWillReceiveProps(){
	}
	initCalendar(){
		this.cal = new Pikaday({ 
			field: this.refs['cal'],
			minDate: this.props.minDate,
			format: 'MMM D, YYYY',
			position: 'right',
			defaultDate: this.props.defaultDate ? this.props.defaultDate : null,
			setDefaultDate: this.props.defaultDate ? this.props.defaultDate : null,
			onSelect: (selected) => {
				this.props.dateOnChange(selected)
			}
		})
	}
	componentWillUnmount(){
		this.cal.destroy()
	}
	disableKeypress(e){
		let key = e.which || e.keyCode
		if(key !== 9) e.preventDefault()
	}
	render(){ 
		return(
			<div className="calendar">
				<input 
					onKeyDown={this.disableKeypress.bind(this)} 
					type="text" 
					ref="cal" 
					className="start-date" 
					tabIndex="6" 
					placeholder={this.props.placeholder} />
			</div>
		)
	}
}

