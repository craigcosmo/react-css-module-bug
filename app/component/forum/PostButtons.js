import React from 'react'
import {Link} from 'react-router'
import 'postButtons.scss'

const PostButtons = (props) => {
	return (
		<div className="post-buttons">
			<button className="ui-submit-btn">create</button>
			<button className="ui-cancel-btn" onClick={props.closeComment}>cancel</button>
		</div>
		)
}

export default PostButtons