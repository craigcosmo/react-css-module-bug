import i from 'actionConstant'
import moment from 'moment'
const defaultState= {
	forumName: '',
	forumPath: '/forum/barista-fair-trade',
	topicCollection: [],
	loadCount: 5,
	event: {
		today : new Date(),
		startDate: new Date(),
		endDate: new Date(),
		startTime : moment().format('h:m:A'),
		endTime : moment().format('h:m:A')
	}
}

export default (state = defaultState, action) => {
	switch(action.type){
		case i.GET_TOPIC_SUCCESS:
		return {...state, topicCollection : action.payload}
		case i.STORE_FORUM_NAME:
		return{...state, forumName: action.payload}
		case i.STORE_FORUM_PATH:
		return{...state, forumPath: action.payload}
		case i.STORE_START_TIME:
		return {
				...state, 
				event: {...state.event, startTime: action.payload}
			}
		case i.STORE_END_TIME:
		return {
			...state,
			event: {...state.event, endTime: action.payload}
		}
		case i.STORE_START_DATE:
		return {
			...state,
			event: {...state.event, startDate: action.payload}
		}
		case i.STORE_END_DATE:
		return {
			...state,
			event: {...state.event, endDate: action.payload}
		}
		case i.STORE_PERIOD:
		return {
			...state,
			event: {...state.event, period: action.payload}
		}
	}
	return state
}
