import React from 'react'
import 'creator.scss'
import CreateDiscussion from 'CreateDiscussion'
import CreateEvent from 'CreateEvent'
import CreatePoll from 'CreatePoll'



export default class Creator extends React.Component{
	constructor(){
		super()
		this.state = {
			tab: 'discussion'
		}
	}
	changeToDiscussion(){
		this.setState({tab: 'discussion'})
	}
	changeToQuestion(){
		this.setState({tab: 'question'})
	}
	changeToEvent(){
		this.setState({tab: 'event'})
	}
	changeToPoll(){
		this.setState({tab: 'poll'})
	}
	render(){ 
		return(
			<div className="creator">
				<div className="tab">
					<span className="tab-item cap" onClick={this.changeToDiscussion.bind(this)}>discussion</span>
					<span className="tab-item cap" onClick={this.changeToQuestion.bind(this)}>question</span>
					<span className="tab-item cap" onClick={this.changeToEvent.bind(this)}>event</span>
					<span className="tab-item cap" onClick={this.changeToPoll.bind(this)}>poll</span>
				</div>
				<div className="creator-content">
					{
						this.state.tab === 'discussion' && 
						<CreateDiscussion 
							submitPost={this.props.submitPost} 
							submitTopic={this.props.submitTopic} />
					}
					{
						this.state.tab === 'event' && 
						<CreateEvent 
							submitPost={this.props.submitPost} 
							submitTopic={this.props.submitTopic} 
							event={this.props.forum.event} />
					}
					{
						this.state.tab === 'poll' && 
						<CreatePoll 
							submitPost={this.props.submitPost} 
							submitTopic={this.props.submitTopic} />
					}
				</div>
				
			</div>
		)
	}
}