import React from 'react'
import Header from 'Header'
import {Link} from 'react-router'
import moment from 'moment'
import {secondsToDate, different} from 'dateHelper'
import Loader from 'Loader'
import 'forum.scss'
import Footer from 'Footer'
import Box from 'Box'
import config from 'config'
import Posting from 'Posting'
import Entry from 'Entry'
import Creator from 'Creator'
import FirstPost from 'FirstPost'
import AllPost from 'AllPost'
import Poll from 'Poll'
import Event from 'Event'
import PollResult from 'PollResult'
import Question from 'Question'
import $ from 'jquery'

export default class Forum extends React.Component {
	constructor(props) {
		super(props)
		this.state ={
			loader : false,
			continuosScrollingLoader : false
		}
		this.count = 5
	}
	componentDidMount(){

		if (this.count ===5) this.setState({loader:true})
		this.props.getTopic(this.count)
		.then( () =>{
			if (this.count ===5) {
				this.setState({loader:false})
				this.auto()
			}
		})
		this.props.storeForumName(this.props.router.params.forum)
		this.props.storeForumPath(this.props.router.params.forum)



	}
	auto(){

		let that = this
		$(window).scroll(function() {
			if($(window).scrollTop() + $(window).height() == $(document).height()) {

				that.setState({continuosScrollingLoader:true})
				that.count = that.count + 5
				that.props.getTopic(that.count)
				.then( function(){
					that.setState({continuosScrollingLoader:false})
				})
				.catch( () => {
					that.setState({continuosScrollingLoader:false})
				})
			}
		})
	}
	renderTopicType(item){
		if(item.type==='discussion'){
			return <FirstPost post={item.post}/>
		}

		if(item.type==='poll' && item.votedByThisUser === true){
			return <PollResult item={item} />
		}
		
		if(item.type==='poll' && item.votedByThisUser === false){
			return <Poll item={item} submitVote={this.props.submitVote} storeCurrentTopic={this.props.storeCurrentTopic}/>
		}

		if(item.type==='event'){
			return <Event item={item} submitAttending={this.props.submitAttending} />
		}
	}
	renderTopic(){
		return this.props.forum.topicCollection.map( (item, index) => (
			<div className="topic-item" key={index}>
				<div className="who-what">
					<span className="user-name cap">{item.userName + ' created a -'}</span>
					<span class={'topic-type ' + 'topic-type-'+item.type}>{item.type}</span>
				</div>
				{item.type==='question' &&
					<Question item = {item} />
				}
				{item.type!=='question'&&
					<p className="topic-title cap"><Link to={'/'+item.topicId+'/'+item.path}>{item.title}</Link></p>
				}

				<div className="user flex">
					<div className="user-left">
						<span className="user-photo"></span>
					</div>
					<div className="user-right">
						<div><span className="user-name capitalize">{item.userName}</span></div>
					</div>
				</div>
				{this.renderTopicType(item)}
				<div className="like-wrapper">
					<span className="created-date">{ different(item.created) } ago</span>
					<span className="hy">-</span>
					<span className="like-btn cap">like</span>
					<span className="hy hide">-</span>
					<span className="reply-btn cap hide">reply</span>
				</div>
				<div className="other-user-post-wrapper">
					<AllPost 
						item={item}
						submitReply={this.props.submitReply}
						/>
					<div className="flex posting-row">
						<div>
							
						</div>
						<Posting 
							item={item} 
							submitPost={this.props.submitPost} />
					</div>
				</div>
			</div>
		))
	}
	render() {
		return (
			<div className="forum pin-footer">
				<Header {...this.props} />
				<main>
					<div className="flex">
						<div className="col1">
							<Box title="online member">

							</Box>
							<Box title="newest member">

							</Box>
						</div>
						<div className="col2">
							<Creator {...this.props} forum={this.props.forum} />
							<div className="topic-collection">
								{this.renderTopic()}
								{this.state.loader && <div><br /><br /><br /><Loader /></div>}
							</div>
							{ this.state.continuosScrollingLoader && <div className="con-loader"><br /><Loader /></div>}
						</div>
						<div className="col3">
							<Box title="upcoming event">
							</Box>
							<Box title="top topics">
							</Box>
						</div>
					</div>
					
				</main>
				<Footer />
			</div>
		)
	}
}
