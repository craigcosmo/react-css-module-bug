import React from 'react'
import Header from 'header/Header'
import config from 'config'
import 'home.scss'

export default class Home extends React.Component {
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<div styleName="home">
				<Header {...this.props} />
				
			</div>
		)
	}
}
