module.exports = {
	plugins: {
		'postcss-assets': {
			loadPaths: ['**']
		},
		'postcss-cssnext': {
			browsers: ['last 2 versions', '> 5%'],
			features: {autoprefixer:false}
		},
	},
}