
## Stack

React, Redux architecture, React-intl, SASS and Webpack as build tool.

## Installation & run

Install `npm` and `nodejs` if you haven't.

Then run the follwoing command

```sh
npm i
```

```sh
npm run build
```

```sh
npm run dev
```

## Test

```sh
npm run test
```



## What happen when build

This build command will generates files like bundle.js, bundle.css, index.html and assets. Those are store in `dist` folder ready to be shipped.

This build will create  one js file and one css file

The css and js file name will have hash as query string

Auto insert css and js link into index.html

When develop in local, if developer change code in webpack.config.js, the dev ser will restart

On each build commnad, the dist folder will be deleted, new content will be created to replace immediately

On each run command, when the run progress complete, a browser window will open

The dev server is webpack dev server instead of a dedicate local node express server

All the asset in image folder and font folder will be copied into it's relavent folders in dist 

## Folder structure 


`app` contains components and logic

`dist` contains the code that is ready to ship

`font` contains all font asset used in the app, if you want to use a font, this is where you add font files to.

`image` contains all image asset used in the app, if you want to use an image in the app, this is where you add image files to, image files can be png, jpg ,gif ,svg.

`node_modules` open source libraries/dependcies that we need to use in the app

`test` this we where we put our test code in.

`translation` this is where we translate english to sweden manually.

`webpack` contains modules that is needed for webpack to run. If you don't understand the code inside this, it's ok, it's not really matter to the outcome of our app. In other, during the entire development process you probably don't need to touch this folder.

`.editorconfig` this file guides your text editor how to auto indent code and indent with how many tab, your edtior setting might have this set up already, but this file will overide your setting. Only if your editor supports this type of file, then it will do the work for you automatically. Or else, you probably don't have to worry about this.

`eslintrc` eslint config file

`eslintignore` contains the entries/files/codes black list that we don't want to lint during linting


`translationRunner.js` this file will compile default messages in component, and line it up nicely in json format, so that we can translate english to sweden easily, instead of typing `hello = hej` we just need to type `hej` the `hello` text is already pre-loaded inside the translation file for us.

`webpack.config.babel.js` contains insutruction of how we want our code to be built. What file name we want it to be, where to we want to save it after build complete, etc...

`webpack.test.config.babel.js` is simliar to `webpack.config.babel.js` but this is for bundling our test code. Why would we to build/bundle our code? there will be a session explain this later.


## Golbal CSS Rules

Exact order

normailize.scss - A collection of HTML element and attribute style-normalizations [http://necolas.github.io/normalize.css/](http://necolas.github.io/normalize.css/)

[global.scss](#global.scss) - specific CSS that is used across the site

[flexboxgrid](http://flexboxgrid.com/) - a grid system made with CSS3 flexbox
globalResponsive - specific responsive styles that are used accross the site

## Global.scss

```
html {font-size: 62.5%; box-sizing: border-box;}
*, *:before, *:after {box-sizing: inherit;}
* {margin: 0;padding: 0;}
// should not set rem on the body refer issue 319623 on chromium google code
body > * {font-size: 1.4rem;font-family: 'Lato';font-weight: 400;}
h1,h2,h3,h4,h5,h6 {font-weight: 400}
h1{font-size: 2.4rem; }
h2{font-size: 2.2rem; }
h3{font-size: 2.0rem; }
h4{font-size: 1.8rem; }
h5{font-size: 1.6rem; }
h6{font-size: 1.4rem; }
a{cursor: default;}
button{border: none; outline: none;background: none}
.inline-block {display: inline-block;}
.unselectable {-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none; user-select: none;}
.center {text-align: center;}
.right{text-align: right;}
.left{text-align: left;}
.middle{vertical-align: middle;}
.relative {position:relative}
.hide {display:none}
.must-hide{display: none !important }
.hidden{visibility: hidden;}
.capitalize {text-transform: capitalize;}
.uppercase {text-transform: uppercase;}
.cap::first-letter{text-transform: capitalize}
.clearfix::after {content: "";display: table;clear: both;}
.padding0{padding:0}
.margin0{margin:0}
.light {font-weight: 400}
.bold{font-weight: 500}
.table{display: table;}
.tcell {display: table-cell;}
.trow{display: table-row;}
.max-width{ width: 100%}
html, body {width: 100%;height: 100%;}
#app {width: 100%;height: 100%; position: relative;}
.page {width: 100%;height: 100%; position:relative;}
body {background: #E9EAEB}
```


## How to create a component

Let's say you want to create a register form component, here is how you do it

`Register.js`

```
import React from 'react'
import 'register.scss'
export default class Register extends React.Component {
	render() {
		return (
			<div className="register">
				<input type="text" name="username"  />
			</div>
		)
	}
}
```

The file name should be capitalized, so we know it's a react . This file will be put anywhere inside the directory app/component. How you want to organize folder structure inside app/component/ is entirely up to you.

## Naming Convention

Component names will start with capitalized letter and then fowllow with cammel case rule. Component file extension is `.js`

CSS file names are lowercased and follow cammel case rule, file extension will be `.scss` whether the content is sass or just css

CSS class selectors are lowercased connected with hyphen 

Folder names follow cammel case rule

Asset like images, fonts follow cammel case rule



## Support

If you have problem running this app, let me know via my email  craigcosmo@gmail.com

## How to use redux in this app

create action
import acction file to mainAction.js
create reducer
import reducer to mainReducer.js
then map state to props
in mainReducer.js, within the combineReducer function, add an entry with your reducer name

## How Add image asset to project

Images asset is store in image folder. You can make sub folder to suit your need.

You need to stop the dev server and run build commmand after you add new images. So the new images will be regconized by the dev server.


## React intl set up

package.json

```json
"babel": {
  "presets": [
    "es2015",
    "react",
    "stage-0"
  ],
  "plugins": [
    [
      "react-intl",
      {
        "messagesDir": "./translation/defaultMessage",
        "enforceDescriptions": false
      }
    ]
  ]
}

```

What you need to install first:

react-intl
react-intl-redux
babel-plugin-react-intl : this will copy the default message in components and put into a respective folder of our choice
react-intl-translations-manager: this will compile the default message into an easy to understand and read presentation. From here we can write the translation for default message. react-intl has it own mechanism to do this but this package did a much better job.

To load the message we include the translated json file in our component, and pass it through as props. Remember to tell react what locale you want and assign relavent json file to that locale before passing props.

react will automatic copy translation base on component into a folder normally called default message, for react int to do so, we need to teach webpack how to do that

```js
import {IntlProvider} from 'react-intl-redux'
import sv from 'sv'

const locale='sv'
const message = sv

<IntlProvider locale={locale} message={message}>
	<Router history={browserHistory}>
		...
	</Router>
</IntlProvider>
```



`enforceDescriptions` is flag to enforce us to give a descript for every translation


## Creating new folder in project

When creating a new folder in the project, the dev server must be restarted. Right now this process is not autmated. Developers need to restart manually.

## Haste file system

This file system basically allow to import file/module by just doing like so

`import Header from 'Header'`

As you can there's is no path needed. Just file name. 

To use this system your file name should be uniqe.

But there is case you have two file with the same name, you can import them like this

`import Header from 'userAccountFolder/Header'`

`import Header from 'adminfolder/Header'`

Just add the path to differencite the two file.

If you have mistakenly imported two file with the same name. Don't worry. Webpack build system will automatically tell you that you have duplicated file name. You can just resolve it by adding the path.





